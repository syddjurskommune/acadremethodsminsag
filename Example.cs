﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcadreMethodsMinSag
{
    class ExampleCode
    {
        public void Example1()
        {
            string AcadreEndpointURL = "https://acadrepws.syddjurs.dk/acadre/Acadre.CM.PublicServices/soapauth/AcadreServiceV7.asmx";
            string Username = "MinSag";
            string Password = "???????";
            string Domain = "Syddjursnet";
            AcadreService acadreService = new AcadreService(AcadreEndpointURL, Username, Password, Domain, Username);

            // Henter alle dokumenter i Acadre, som er omfattet af aktindsigt på borgeren
            IEnumerable<CaseDocument> caseDocuments = acadreService.GetCaseDocuments("1111111111");
            // ELLER
            // man kan vælge at hente metadata først (CaseDocument.DocumentContent = null) og derefter hente dokumentindholdet enkeltvis for at undgå at belaste arbejdshukommelsen
            IEnumerable<CaseDocument> metacaseDocuments = acadreService.GetCaseDocumentsMetadata("0102030405");
            foreach (var metaCaseDocument in metacaseDocuments)
            {
                byte[] documentContent = acadreService.GetCaseDocumentContent(metaCaseDocument);
                // Gør noget med dokumentindholdet (filtype fremgår af filnavn: metaCaseDocument.Filename)
            }
        }
    }
}
