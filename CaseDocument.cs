﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcadreMethodsMinSag
{
    public class CaseDocument
    {
        public CaseDocument()
        {
            DocumentMetaData = new AcadreServiceV7.DocumentResponseType2();
            CaseMetaData = new AcadreServiceV7.CaseFileType3();
        }
        public AcadreServiceV7.DocumentResponseType2 DocumentMetaData;
        public AcadreServiceV7.CaseFileType3 CaseMetaData;
        public string Filename;
        public byte[] DocumentContent;
    }
}
