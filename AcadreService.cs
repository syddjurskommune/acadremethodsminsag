﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AcadreMethodsMinSag
{
    public class AcadreService
    {
        private AcadreServiceV7.CaseService7 caseService;
        private AcadreServiceV7.ContactService7 contactService;
        private AcadreServiceV7.MainDocumentService7 documentService;
        private AcadreServiceV4.DocumentService4 documentService4;
        string[] CaseFileTypeCodes = { "BGSAG" , "BUSAG" };
        // returns all documents on citizen with content.
        public AcadreService(string Acadre_url, string Acadre_username, string Acadre_password, string Acadre_domain, string Acadre_actingForUsername)
        {
            Acadre.PWSHeaderExtension.User = Acadre_actingForUsername;
            System.Net.NetworkCredential networkCredential = new System.Net.NetworkCredential(Acadre_username, Acadre_password, Acadre_domain);
            caseService = new AcadreServiceV7.CaseService7
            {
                Credentials = networkCredential,
                Url = Acadre_url
            };
            contactService = new AcadreServiceV7.ContactService7
            {
                Credentials = networkCredential,
                Url = Acadre_url
            };
            documentService = new AcadreServiceV7.MainDocumentService7
            {
                Credentials = networkCredential,
                Url = Acadre_url
            };
            documentService4 = new AcadreServiceV4.DocumentService4
            {
                Credentials = networkCredential,
                Url = Acadre_url.Replace("7.asmx", "4.asmx")
            };
        }
        public IEnumerable<CaseDocument> GetCaseDocuments(string CPR)
        {
            List<CaseDocument> caseDocuments = new List<CaseDocument>();
            AcadreServiceV7.AdvancedSearchCaseCriterionType2 searchCaseCriterion = new AcadreServiceV7.AdvancedSearchCaseCriterionType2();
            CPR = CPR.Replace("-", "").Trim();
            if (!IsValidCPR(CPR))
                throw new Exception("Input argument is not recognised as a CPR-number");
            searchCaseCriterion.CaseFileTitleText = CPR;
            foreach (var CaseFileTypeCode in CaseFileTypeCodes)
            {
                searchCaseCriterion.CaseFileTypeCode = CaseFileTypeCode;
                var foundCases = caseService.SearchCases(searchCaseCriterion);
                foreach (var foundCase in foundCases)
                {
                    var Case = caseService.GetCase(foundCase.CaseFileReference);

                    var foundDocuments = documentService.GetAllDocuments(foundCase.CaseFileReference);
                    foreach (var foundDocument in foundDocuments)
                    {
                        AcadreServiceV4.FileVersionReferenceType fileVersionReference = new AcadreServiceV4.FileVersionReferenceType()
                        {
                            FileReference = foundDocument.DocumentVersion.DocumentFileReference,
                            Version = foundDocument.DocumentVersion.DocumentVersionIdentifier
                        };
                        caseDocuments.Add(new CaseDocument
                        {
                            CaseMetaData = Case,
                            DocumentMetaData = foundDocument,
                            Filename = documentService4.GetFileName(fileVersionReference),
                            DocumentContent = documentService4.GetPhysicalDocument(fileVersionReference)
                        });
                    }
                }
            }
            return caseDocuments;
        }
        // returns all documents on citizen but only metadata (CaseDocument.DocumentContent is null).
        // The actual content can be retrieved with method GetCaseDocumentContent() using CaseDocument as input argument
        public IEnumerable<CaseDocument> GetCaseDocumentsMetadata(string CPR)
        {
            List<CaseDocument> caseDocuments = new List<CaseDocument>();
            AcadreServiceV7.AdvancedSearchCaseCriterionType2 searchCaseCriterion = new AcadreServiceV7.AdvancedSearchCaseCriterionType2();
            CPR = CPR.Replace("-", "").Trim();
            if (!IsValidCPR(CPR))
                throw new Exception("Input argument is not recognised as a CPR-number");
            searchCaseCriterion.CaseFileTitleText = CPR;
            foreach (var CaseFileTypeCode in CaseFileTypeCodes)
            {
                searchCaseCriterion.CaseFileTypeCode = CaseFileTypeCode;
                var foundCases = caseService.SearchCases(searchCaseCriterion);
                foreach (var foundCase in foundCases)
                {
                    var Case = caseService.GetCase(foundCase.CaseFileReference);

                    var foundDocuments = documentService.GetAllDocuments(foundCase.CaseFileReference);
                    foreach (var foundDocument in foundDocuments)
                    {
                        AcadreServiceV4.FileVersionReferenceType fileVersionReference = new AcadreServiceV4.FileVersionReferenceType()
                        {
                            FileReference = foundDocument.DocumentVersion.DocumentFileReference,
                            Version = foundDocument.DocumentVersion.DocumentVersionIdentifier
                        };
                        caseDocuments.Add(new CaseDocument
                        {
                            CaseMetaData = Case,
                            DocumentMetaData = foundDocument,
                            Filename = documentService4.GetFileName(fileVersionReference)
                        });
                    }
                }
            }
            return caseDocuments;
        }
        // returns document content
        public byte[] GetCaseDocumentContent(CaseDocument caseDocument)
        {
            if (caseDocument.DocumentContent == null)
            {
                AcadreServiceV4.FileVersionReferenceType fileVersionReference = new AcadreServiceV4.FileVersionReferenceType()
                {
                    FileReference = caseDocument.DocumentMetaData.DocumentVersion.DocumentFileReference,
                    Version = caseDocument.DocumentMetaData.DocumentVersion.DocumentVersionIdentifier
                };
                return documentService4.GetPhysicalDocument(fileVersionReference);
            }
            else
                return caseDocument.DocumentContent;
        }
        private static bool IsValidCPR(string cpr)
        {
            if (cpr.Length != 10) return false;
            uint theNum = 0;
            if (!uint.TryParse(cpr, out theNum)) return false;
            if (int.Parse(cpr.Substring(0, 2)) > 31 || int.Parse(cpr.Substring(0, 2)) == 0) return false;
            if (int.Parse(cpr.Substring(2, 2)) > 12 || int.Parse(cpr.Substring(2, 2)) == 0) return false;
            return true;
        }
    }
}
